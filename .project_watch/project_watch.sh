#!/bin/sh

# load config
. $(pwd)/config.rc




while true

do 
	file=$(inotifywait -r `echo $conf_project`) 
	rsync -rtvz --delete --exclude=.git/ --exclude=.project_watch/ `echo $conf_project` `echo $conf_login`':'`echo $conf_remote`

#set notification

if [ $(echo $conf_notification)  == "yes" ];

then

echo $file | awk -F' ' '{print $2 " => "  $3}' | while read OUTPUT; do notify-send "$OUTPUT";
done
fi


done


