irsync
======
This is a very simple solution for auto sync project (originally used with Vim).

irsync
======

Preinstallation
---------------

Install inotify-tools, rsync, notify-send

In this scenario a passwordless ssh connection is set. (I recommend to do that, a password prompt case was not tested). 
If you have already your keys generated, execute this command: 

> ssh-copy-id yourusername@remote.server

If your local project is empty, copy first, from the remote, the files you want to work with.

Usage
-----
Copy folder .project_watch to your root project. 

Make sure to set your details in config.rc

> # project directory
> conf_project="/your/project/path/"
> # remote directory
> conf_remote="/somewhere/beyond/the/sea/project/path/"
> # ssh login
> conf_login="yourlogin@host.in.da.house"
> #set notification
> conf_notification="yes"

Run: bash ./project_watch.sh


Description
-----------

The script will be monitoring the places, until it is interrupted. 
By default, folders .git and .project_watch are being excluded. 

Tested on debian beased distros. 



